# Tojatta Angles

This is a very basic angle calculation/vector library I made, and just thought I should share it. Please enjoy!

## Getting Started

How to get this set-up and implemented.

### Download

[Latest](https://mega.nz/#!odVQASQJ!Bay4sBKhiZb8WVZfudlJcgZv2T6QFEf78EokXxf5VQQ)

### Installing

With the downloaded library, add it to your class path. Simple enough? 

### Example Steps


1. First off make a instance of the angle utility
The parameters for AngleUtility are
minYawSmoothing, maxYawSmoothing, minPitchSmoothing, maxPitchSmoothing
```
private AngleUtility angleUtility = new AngleUtility(10, 40, 10, 40);
```


2. Inside your pre player update event you want to use my vectors to hold the entity's positions (After you find your target entity)
```
Vector3<Double> enemyCoords = new Vector3<>(target.posX, target.posY, target.posZ);
Vector3<Double> myCoords = new Vector3<>(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ);
```


3. Now you will want to use those vectors and calculate a angle using them (Skip to step 6 if you don't wanna smooth)
```
Angle dstAngle = aimUtility.calculateAngle(enemyCoords, myCoords);
```


4. The next few steps are optional, If you want smoothing. 
You want to gather your current angles
I use serverAngles field to keep track of where I'm looking server sided.
It's not nessisary but it helps to make your aura/aimbot more effective.
If you don't want to, just replace with...
mc.thePlayer.rotationYaw and mc.thePlayer.rotationPitch
```
Angle srcAngle = new Angle(serverAngles.getYaw(), serverAngles.getPitch());
or
Angle srcAngle = new Angle(mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
```


5. We have to calculate the smoothed angle now using the angles we currently have (from 3 and 4)
```
Angle smoothedAngle = aimUtility.smoothAngle(dstAngle, srcAngle);
```


6. Now we can finally set our calculated angles
```
Non-Smoothed:
event.setYaw(lockview ? mc.thePlayer.rotationYaw = dstAngle.getYaw() : dstAngle.getYaw());
event.setPitch(lockview ? mc.thePlayer.rotationPitch = dstAngle.getPitch() : dstAngle.getPitch());
Smoothed:
event.setYaw(lockview ? mc.thePlayer.rotationYaw = smoothedAngle.getYaw() : smoothedAngle.getYaw());
event.setPitch(lockview ? mc.thePlayer.rotationPitch = smoothedAngle.getPitch() : smoothedAngle.getPitch());
```